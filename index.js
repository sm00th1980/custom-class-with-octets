import trim from 'underscore.string/trim';
import _ from 'lodash';

export const SEPARATOR = '**';

export const OCTET = {FIRST: 0, SECOND: 1};

const stripKlass = (input, klass) => {
	if (input) {
		return input.split(' ').filter(item => item !== klass).join(' ');
	}

	return '';
};

const withoutSpecialKlass = (source, klass, octet) => {
	if (source) {
		return source.split(SEPARATOR).map((element, index) => {
			if (index === octet) {
				return stripKlass(trim(element), klass);
			}

			return trim(element);
		}).join(` ${SEPARATOR} `);
	}

	return '';
};

const withSpecialKlass = (source, klass, octet) => {
	if (source) {
		return withoutSpecialKlass(source, klass, octet).split(SEPARATOR).map((element, index) => {
			if (index === octet) {
				return `${trim(element)} ${klass}`;
			}

			return trim(element);
		}).join(` ${SEPARATOR} `);
	}

	return '';
};

const toggleClass = (klass, octet, element, condition) => {
	const sourceKlass = element.className;
	const result = condition ? withSpecialKlass(sourceKlass, klass, octet) : withoutSpecialKlass(sourceKlass, klass, octet);
	element.className = result;
};

const _compose2 = (...args) => {
	const args_ = _.initial(args);
	const action_ = _.last(args);

	return klass => {
		return {
			[args_[0]]: octet => {
				return {
					[args_[1]]: element => {
						action_(klass, octet, element);
					}
				};
			}
		};
	};
};

const _compose3 = (...args) => {
	const args_ = _.initial(args);
	const action_ = _.last(args);

	return klassToRemove => {
		return {
			[args_[0]]: klassToAdd => {
				return {
					[args_[1]]: octet => {
						return {
							[args_[2]]: element => {
								action_(klassToRemove, octet, element, false);
								action_(klassToAdd, octet, element, true);
							}
						};
					}
				};
			}
		};
	};
};

const _composeWithCondition = (...args) => {
	const args_ = _.initial(args);
	const action_ = _.last(args);

	return klass => {
		return {
			[args_[0]]: octet => {
				return {
					[args_[1]]: element => {
						return {
							when: condition => {
								action_(klass, octet, element, condition);
							},
							unless: condition => {
								action_(klass, octet, element, !condition);
							}
						};
					}
				};
			}
		};
	};
};

export default {
	withoutSpecialKlass,
	withSpecialKlass,
	toggleClass: _composeWithCondition('intoOctet', 'applyToElement', toggleClass),
	addClass: _compose2('intoOctet', 'applyToElement', _.partial(toggleClass, _, _, _, true)),
	removeClass: _compose2('intoOctet', 'applyToElement', _.partial(toggleClass, _, _, _, false)),
	replaceClass: _compose3('toClass', 'intoOctet', 'applyToElement', toggleClass)
};